def SplitPoint(a, n): 

    lsum =0

    for i in range(0,n):
        lsum += a[i] 
        rSum =0
        for j in range(i+1, n): 
            rsum += a[j] 

        if (lsum == rsum): 
           return i+1
    return-1


def printWeightedpoints(a, n): 
    
    split = SplitPoint(a, n) 
    
    if (split ==-1 or split == n ): 
        print ("Not Possible") 
        return
    for i in range(0, n): 
        if(split == i): 
            print ("") 
    print (str(a[i]) +' ', end=" ") 
a = [1,7,8,2] 
n =len(a) 
printWeightedpoints(a, n)

