import random
class Computershop:
    
    def __init__(self,name,amounts = 0):
        self.name = name
        self.amounts = amounts

    def generate_amount(self):
        ram_computer  = Amountcal.ram_computer()
        license_computer_shop = Amountcal.license_computer_shop()
        quality_factor = Amountcal.quality_factor()
        tax_computer_shop = Amountcal.tax_computer_shop()

        self.amounts = (ram_computer + quality_factor + tax_computer_shop)/3 - license_computer_shop


class Amountcal:
    
    @staticmethod
    def ram_computer():
        return random.randint(4,16)

    @staticmethod
    def license_computer_shop():
        return random.randint(0,15)    

    @staticmethod
    def quality_factor():
        return random.randint(50,80)    
    
    @staticmethod
    def tax_computer_shop():
        return random.randint(5,15)    

class Computer:
    computer_names = ('Dell','Hp','lenovo','Asus','Mac')
    computers = []

    @classmethod
    def run(cls):
        for comp in cls.computer_names:
            computer = Computershop(comp)
            
            computer.generate_amount()
            cls.computers.append(computer)

            # print(computer.amounts, computer.name)

        # cls.computers.sort(key=lambda computer: computer.amount, reverse=True)

        for computer in cls.computers:
            print(computer.name, computer.amounts)


Computer.run()