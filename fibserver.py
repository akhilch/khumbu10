import socket
from finonacci import fibonacci

s = socket.socket()	
port = 12348

# fix to reuse the same port on everytime running
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

s.bind(('', port))
s.listen(5)

while True:
    c, addr = s.accept()	 

    # send data
    
    max = int(c.recv(1024).decode())

    c.send((max.encode())
    c.close()
